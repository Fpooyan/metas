# This function gets attack sample centers as input and randomly generates neg_centers(not overlapping with attack samples)

import cv2
import os
import random
import numpy as np
import pickle
import json

def is_valid(h,w,frame,winsize,numofframes,step_size,height,width,numframes):
    winsize=int(winsize)
    if h>int(winsize/2) and h+int(winsize/2)+1<height and w>int(winsize/2) and w+int(winsize/2)+1<width and frame>int(numofframes/2)*step_size and frame+int(numofframes/2)*step_size<numframes:
        return True
    return False



def neighbouring(l1,l2,winsize,numofframes,step_size):
    winsize=int(winsize)
    #print("neg",l1)
    #print("pos",l2)
    if l1[0] in range(l2[0]-int(winsize),l2[0]+int(winsize)) and l1[1] in range(l2[1]-int(winsize),l2[1]+int(winsize)) and l1[2] in range(l2[2]-int(numofframes*step_size),l2[2]+int(numofframes*step_size)):
        return True
    return False

def save_neg_centers(path,stagenum, dataset_path,video_name,ext,winsize,numofframes,neg_num,step_size,neg_path):
    if os.path.exists(os.path.join(neg_path,video_name+"_negative_centers.json")):
        print("Negative centers have already been generated for this video!")
    else:
        vid = cv2.VideoCapture(os.path.join(dataset_path,  video_name + ext))
        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        numframes = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
        #print(width,height,numframes)
        attacks_centers = []

        attackspath = os.path.join(path,'stage_'+str(stagenum), 'centers', 'Pos',video_name+"_attack.json")
        if os.path.exists(attackspath):
            with open(attackspath,"r") as f:
                    attacks=json.load(f)
            for attack in attacks.keys():
                x,y,t = attacks[attack]
    
                #x = int(center.split(',')[0])     #width
                #y = int(center.split(',')[1])     #height
                #t = int(center.split(',')[2])     #time
                if y < int(winsize / 2):
                    y = int(winsize / 2)
                if y + int(winsize / 2) >= height:
                    y = height - int(winsize / 2) - 1
                if x < int(winsize / 2):
                    x = int(winsize / 2)
                if x + int(winsize / 2) >= width:
                    x = width - int(winsize / 2) - 1
                if t < numofframes / 2:
                    t = numofframes / 2
                if t + numofframes / 2 > numframes:
                    t = numframes - numofframes / 2 - 1
                attacks_centers.append([int(x),int(y),int(t)])
            inds = {}
            count = 0
            while (count < neg_num):
                w = np.random.randint(0, width - 1)
                h = np.random.randint(0, height - 1)
                frame = np.random.randint(0, numframes - 1)
                if not is_valid(h,w,frame,winsize,numofframes,step_size,height,width,numframes):
                    continue
                flag = False
                for attack in attacks_centers:
                    if (neighbouring([w,h,frame],attack,winsize,numofframes,step_size)):
                        flag=True
                if flag==False:
                    inds[str(count)]=[w,h,frame]
                    count+=1
    
            with open(os.path.join(neg_path, video_name+"_negative_centers.json"),'w') as f:
                json.dump(inds,f)
                #for i in inds:
                #    f.write(str(i[0])+','+str(i[1])+','+str(i[2])+"\n")
  
        else:
            inds = {}
            count = 0
            while (count < neg_num):
                w = np.random.randint(0, width - 1)
                h = np.random.randint(0, height - 1)
                frame = np.random.randint(0, numframes - 1)
                if not is_valid(h,w,frame,winsize,numofframes,step_size,height,width,numframes):
                    continue
                inds[str(count)]=[w, h, frame]
                count+=1
    
            with open(os.path.join(neg_path, video_name+"_negative_centers.json"),'w') as f:
                json.dump(inds,f)
                #for i in inds:
                #    f.write(str(i[0])+','+str(i[1])+','+str(i[2])+"\n")

            
#save_neg_centers('P:\\metas_new_codes\\dataset','t1',5,5,3,2,"P:\\metas_new_codes\\dataset")

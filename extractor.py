# This function gets sample centers as input and crops the sample from the specified video
# Important parameters:  
#               winsize: The size of cropped square (each frame is considered as a square)
#               numofframes: The number of frames (video length)
#               step_size: sampling rate of frames
import cv2
import numpy as np
import os
import json


def extractor(dataset_path,video_name,ext,centers_file,winsize,numofframes,step_size,outpath):
    invals = []
    if os.path.exists(os.path.join(outpath, video_name)):
        print("This extraction is available!")
    else:
        vid = cv2.VideoCapture(os.path.join(dataset_path,  video_name + ext))

        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        numframes = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
 
        # Reading the centers from txt file
        centers = {}
        with open(centers_file,"r") as f:
            #centers_list = f.readlines()
            centers = json.load(f)
        #for line in centers_list:
        #    centers.append([int(line.split(',')[0]),int(line.split(',')[1]),int(line.split(',')[2])])
        # Create samples
        valid_centers={}
        for cent in centers.keys():
            if centers[cent][1]>int(winsize/2) and centers[cent][1]+int(winsize/2)<height and centers[cent][0]>int(winsize/2) and centers[cent][0]+int(winsize/2)<width and centers[cent][2]>int(numofframes/2)*step_size and centers[cent][2]+int(numofframes/2)*step_size<numframes:
                valid_centers[cent]=centers[cent]
        print("valid cen len",len(valid_centers.keys()))
        cc = np.zeros((len(centers.keys())), dtype=int) # frame indices
        for i in range(int(numframes)):
            r, img = vid.read()
            flag=True
            indices = [j for j in valid_centers.keys() if i in range(valid_centers[j][2]-int(numofframes/2)*step_size,valid_centers[j][2]+int(np.ceil(numofframes/2))*step_size,step_size)]
            for ind in indices:
                if not os.path.exists(os.path.join(outpath, video_name, ind)):
                    os.makedirs(os.path.join(outpath, video_name, ind))
                temp = img[valid_centers[ind][1]-int(winsize/2):valid_centers[ind][1]+int(np.ceil(winsize/2)),valid_centers[ind][0]-int(winsize/2):valid_centers[ind][0]+int(np.ceil(winsize/2)),:]
                flag=cv2.imwrite(os.path.join(outpath, video_name, ind, str(cc[int(ind)])+".jpg"),temp)
                cc[int(ind)]+=1
                if flag==False:
                     print(video_name,ind)
                     if os.path.join(outpath, video_name, ind) not in invals:
                         invals.append(os.path.join(outpath, video_name, ind))
    #print(invals)
    # added by zahra
    import shutil
    for inval in invals:
        shutil.rmtree(inval)
#extractor('P:\\metas_new_codes\\dataset',"t1","P:\\metas_new_codes\\dataset\\t1_negative_centers.json",5,5,2,"P:\\metas_new_codes\\dataset")

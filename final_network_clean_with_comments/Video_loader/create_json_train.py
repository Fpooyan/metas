import numpy as np
import os
import math
import json

"""
Parameters that should be set

vals=["48","52","57","59","64","67","68","21"]   #list of validation video numbers
healthy_percents=[100,50,000] # Just an example
n_aug_train_healthy=[0,0,0]
main_dir="" # The experiment folder

"""

"""
This code generates the dataset jsons as follows:

    data_json: a dictionary with keys:[train ,val] and the list of corresponding sample IDs for each key
    ID_json: a dictionary which maps each sample ID with its target label
    len_json: a dictionary which maps each sample ID with its number of frames
"""

def create_jsons(vals,healthy_percents,n_aug_train_healthy,main_dir):
    print(healthy_percents)
    data_json={}
    ID_json={}
    len_json={}
    """
    train_list: a list that contains all the training samples IDs
    val_list: a list that contains all the validation samples IDs
    """
    train_list=[]
    val_list=[]
    stage_numbers=len(healthy_percents)
    c_pos_train=0
    c_neg_train=0
    c_pos_val=0
    c_neg_val=0
    """
    This loop does the same json update for the healthy samples
    """
    n_healthy_train=0
    for n_stage in range(0,stage_numbers):
        main_dir_B = main_dir+'/stage_'+str(n_stage)+'/extracted/Neg'
        dirs = [d for d in os.listdir(main_dir_B) if os.path.isdir(os.path.join(main_dir_B, d))]
        ### Count the numbers of negative samples in stage
        n_healthy_train_stage = 0
        for dirr in dirs:
            neg_extraction_list = os.listdir(os.path.join(main_dir_B,dirr))
            for neg_extracted in neg_extraction_list:
                nnn = dirr.split(')')[0]
                if nnn not in vals:
                    n_healthy_train_stage+=1
        # Choose desired percent randomly
        np.random.seed(n_stage)
        index=np.random.permutation(n_healthy_train_stage)
        selected_list=index[0:int(n_healthy_train_stage*healthy_percents[n_stage]/100)]
        ### main for
        select_counter=0
        for dirr in dirs:
            neg_extraction_list = os.listdir(os.path.join(main_dir_B,dirr))
            for neg_extracted in neg_extraction_list:
                p = main_dir_B + '/' + dirr + '/' + neg_extracted
                matches = []
                for root, dirnames, filenames in os.walk(p):
                    for filename in filenames:
                        if filename.endswith(".jpg"):
                            matches.append(os.path.join(root, filename))

                n_frame=len(matches)
                ID=p # ID is the address of sample folder
                label = 0
                ID_json.update({ID: label})
                len_json.update({ID: n_frame})
                nnn=dirr.split(')')[0]
                if nnn in vals:
                    val_list.append(ID)
                    c_neg_val+=1
                if nnn not in vals:
                    if select_counter in selected_list:
                        train_list.append(ID)
                        n_healthy_train+=1
                        c_neg_train+=1
                        for j in range(0, n_aug_train_healthy[n_stage]):
                            ID = 'aug' + str(j) + '__' + p
                            label = 0
                            ID_json.update({ID: label})
                            len_json.update({ID: n_frame})
                            train_list.append(ID)
                            n_healthy_train+=1
                            c_neg_train+=1
                    select_counter+=1


    """
    Calculate number of augments for attacks
    """
    number_of_attacks_train=0
    for n_stage in range(0,stage_numbers):
        main_dir_A = main_dir+'/stage_'+str(n_stage)+'/extracted/Pos'
        dirs = [d for d in os.listdir(main_dir_A) if os.path.isdir(os.path.join(main_dir_A, d))]
        for dirr in dirs:
            pos_extraction_list = os.listdir(os.path.join(main_dir_A,dirr))
            for pos_extracted in pos_extraction_list:
                nnn = dirr.split(')')[0]
                if nnn not in vals:
                    number_of_attacks_train+=1
    print(n_healthy_train,number_of_attacks_train)
    n_aug=int(n_healthy_train/number_of_attacks_train)
    n_aug_train_attack=np.zeros([stage_numbers,],dtype=int)+n_aug

    """
    This loop updates each json with its values for cancer samples
    """
    for n_stage in range(0,stage_numbers):
        main_dir_A = main_dir+'/stage_'+str(n_stage)+'/extracted/Pos'
        dirs = [d for d in os.listdir(main_dir_A) if os.path.isdir(os.path.join(main_dir_A, d))]
        for dirr in dirs:
            pos_extraction_list = os.listdir(os.path.join(main_dir_A,dirr))
            for pos_extracted in pos_extraction_list:
                p = main_dir_A + '/' + dirr + '/' + pos_extracted
                matches = []
                for root, dirnames, filenames in os.walk(p):
                    for filename in filenames:
                        if filename.endswith(".jpg"):
                            matches.append(os.path.join(root, filename))

                n_frame=len(matches)
                ID=p
                label = 1
                ID_json.update({ID: label})
                len_json.update({ID: n_frame})
                nnn=dirr.split(')')[0]
                if nnn in vals:
                    val_list.append(ID)
                    c_pos_val+=1
                if nnn not in vals:
                    train_list.append(ID)
                    c_pos_train+=1
                    for j in range(0, n_aug_train_attack[n_stage]):
                        ID = 'aug' + str(j) + '__' + p
                        label = 1
                        ID_json.update({ID: label})
                        len_json.update({ID: n_frame})
                        train_list.append(ID)
                        c_pos_train+=1



    print(len(train_list),len(val_list))
    print('val positives:',c_pos_val)
    print('val negatives:',c_neg_val)
    print('train positives:',c_pos_train)
    print('train negatives:',c_neg_train)
    print(n_aug_train_attack)
    data_json.update({'train':train_list,'val':val_list})
    out_path=main_dir+'/stage_'+str(stage_numbers-1)+'/dataset_jsons'
    with open(out_path+'/'+'ID_json.json', 'w') as fp:
       json.dump(ID_json, fp)
    with open(out_path+'/'+'data_json.json', 'w') as fp:
       json.dump(data_json, fp)
    with open(out_path+'/'+'LEN_json.json', 'w') as fp:
       json.dump(len_json, fp)













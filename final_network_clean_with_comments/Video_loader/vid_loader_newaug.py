import torch
from torch.utils import data
from PIL import Image
import os
from torch.autograd import Variable
import random
from imgaug import augmenters as iaa
import numpy as np
import cv2
from vidaug import augmentors as va

"""
This code implements the custom dataloader and augmentations
Augmentations are implemented based on imgaug library
Augmentation functions are randomly performed as follows:

    Random changing of blurriness during sample
    Gaussian_blur
    Intensity multiplication
    Intensity addition
    Sharpening
    Rotation
    Flip (horizental,vertical)
    Rescaling
    Translation
"""

"""
Translation augmentation function
"""


def aug_trnslate_frame(img, flag, a, b):
    img_shape = img.shape
    images = np.random.randint(0, 1, (1, img_shape[0], img_shape[1], img_shape[2]), dtype=np.uint8)
    images[0] = img
    seq1 = iaa.Sequential([

        iaa.Sometimes(flag, iaa.Affine(translate_px={"x": (a, a), "y": (b, b)}, mode='wrap'))])
    images_aug = seq1.augment_images(images)
    img_agu1 = images_aug[0]
    return img_agu1


def vid_trnslate_aug(vid, flag, a, b):
    aug_vid = []
    for frame in vid:
        aug_vid.append(aug_trnslate_frame(frame, flag, a, b))
    return aug_vid


"""
Rescaling augmentation function
"""


def aug_scl_frame(img, flag, a):
    img_shape = img.shape
    images = np.random.randint(0, 1, (1, img_shape[0], img_shape[1], img_shape[2]), dtype=np.uint8)
    images[0] = img
    seq1 = iaa.Sequential([

        iaa.Sometimes(flag, iaa.Affine(scale=a, mode='wrap'))])
    images_aug = seq1.augment_images(images)
    img_agu1 = images_aug[0]
    return img_agu1


def vid_scl_aug(vid, flag, a):
    aug_vid = []
    for frame in vid:
        aug_vid.append(aug_scl_frame(frame, flag, a))
    return aug_vid


"""
Rotation augmentation function
"""


def aug_rot_frame(img, flag, a):
    img_shape = img.shape
    images = np.random.randint(0, 1, (1, img_shape[0], img_shape[1], img_shape[2]), dtype=np.uint8)
    images[0] = img
    seq1 = iaa.Sequential([

        iaa.Sometimes(flag, iaa.Affine(rotate=a, mode='wrap'))])
    images_aug = seq1.augment_images(images)
    img_agu1 = images_aug[0]
    return img_agu1


def vid_rot_aug(vid, flag, a):
    aug_vid = []
    for frame in vid:
        aug_vid.append(aug_rot_frame(frame, flag, a))
    return aug_vid


"""
Sharpening augmentation function
"""


def aug_Sharp_frame(img, flag, a, b):
    img_shape = img.shape
    images = np.random.randint(0, 1, (1, img_shape[0], img_shape[1], img_shape[2]), dtype=np.uint8)
    images[0] = img
    seq1 = iaa.Sequential([

        iaa.Sometimes(flag, iaa.Sharpen(alpha=(a, a), lightness=(b, b)))])
    images_aug = seq1.augment_images(images)
    img_agu1 = images_aug[0]
    return img_agu1


def vid_Sharp_aug(vid, flag, a, b):
    aug_vid = []
    for frame in vid:
        aug_vid.append(aug_Sharp_frame(frame, flag, a, b))
    return aug_vid


"""
Performing all augmentation functions on a stack of frames sequentially
"""
def vid_aug(vid, b_f):
    v_add = random.uniform(-15, 40)
    v_mult = random.uniform(0.8, 1.2)
    g_sigma = random.uniform(0, 2)
    """
    This block synthesizes the random change of blurriness in original training samples.
    """
    if b_f:
        global blur_count
        blur_count += 1
        vidf = []
        blur_frame = np.random.randint(4, 10)
        g_sigma1 = random.uniform(0, 2)
        seq1 = va.Sequential([
            va.Sometimes(1, va.GaussianBlur(sigma=g_sigma1)),
            va.Sometimes(1, va.Multiply(v_mult)),
            va.Sometimes(1, va.Add(int(v_add)))])
        vidf[:blur_frame] = seq1(vid[:blur_frame])
        g_sigma2 = random.uniform(0, 2)
        seq2 = va.Sequential([
            va.Sometimes(1, va.GaussianBlur(sigma=g_sigma2)),
            va.Sometimes(1, va.Multiply(v_mult)),
            va.Sometimes(1, va.Add(int(v_add)))])
        vidf[blur_frame:] = seq2(vid[blur_frame:])
        vid = vidf
    else:
        seq1 = va.Sequential([
            va.Sometimes(1, va.GaussianBlur(sigma=g_sigma)),
            va.Sometimes(1, va.Multiply(v_mult)),
            va.Sometimes(1, va.Add(int(v_add)))])
        vid = seq1(vid)

    a = random.uniform(0, 1)
    b = random.uniform(1, 1)
    vid = vid_Sharp_aug(vid, 1, a, b)

    rot_deg = random.uniform(-180, 180)
    vid = vid_rot_aug(vid, 1, rot_deg)
    hf = np.random.randint(0, 2)
    vf = np.random.randint(0, 2)
    seq2 = va.Sequential([
        va.Sometimes(hf, va.HorizontalFlip()),
        va.Sometimes(vf, va.VerticalFlip()),
    ])

    vid = seq2(vid)
    a = random.uniform(0.75, 1.25)
    vid = vid_scl_aug(vid, 1, a)
    a = int(random.uniform(-15, 15))
    b = int(random.uniform(-15, 15))
    vid = vid_trnslate_aug(vid, 1, a, b)

    return vid, rot_deg, hf, vf



blur_count = 0


class Dataset(data.Dataset):
    'Characterizes a dataset for PyTorch'

    def __init__(self, list_IDs, labels, transform, lens, phase, params):
        'Initialization'
        self.labels = labels
        #self.dirs = dirs
        self.list_IDs = list_IDs
        self.transform = transform
        self.win_s = params['winsize']
        self.frame_number = params['numofframes']
        self.step_size = int(params['step_size'])
        self.step_size_in = int(params['step_size'])
        self.right_penlaty = 8
        self.left_penlaty = 8
        self.vid_lengths = lens
        self.phase = phase


    def __len__(self):
        'Denotes the total number of samples'
        return len(self.list_IDs)

    def __getitem__(self, index):
        'Generates one sample of data'

        self.step_size = self.step_size_in
        ID = self.list_IDs[index]    #ID of the sample


        blur_flag = False     #flag for random blur augmentation
        #name = ID
        p = ID
        aug_flag = False      #augmentation flag

        """
        preform both spatial and temporal augmentation
        """
        if ID[0:3] == 'aug':
            aug_flag = True
            #name = ID[(ID.find('_') + 1):]
            p = ID.split('__')[1]
            if int(self.labels[ID]) == 0:
                r = np.random.rand()
                blur_flag = 0.3 < r and r < 0.6

        #p = self.data_folder + '/' + self.dirs[ID] + '/' + name
        n = self.vid_lengths[ID]
        video = []

        y = int(self.labels[ID])
        #a_f = self.activ[ID]
        a_f=int(n/2)
        if aug_flag and self.phase == 'train':
            'random step size between video frames for temporal augmentation implementation'
            self.step_size = round(random.uniform(1.5, 2.5), 1)
            indexs = self.get_subindex(n, y, a_f, ID)
            for ind in indexs:
                video.append(cv2.imread(p + '/' + str(ind) + '.jpg'))

            'temporal augmentation implemented with linear interpolation'
            y = int(self.labels[ID])
            n = len(video)
            ti = np.array([i * (n - 1) / (self.frame_number - 1) for i in range(self.frame_number)])
            res_new = []
            for i in range(len(video) - 1):
                b = video[i]
                a = video[i + 1]
                t = ti[np.logical_and(ti >= i, ti < i + 1)]
                res_new.extend([(j - i) * a + (1 - j + i) * b for j in t])

            res_new.append(video[-1])

            'spatial augmentation'
            VV, rot_deg, hf, vf = vid_aug(res_new, blur_flag)
            

        #No agmnetation
        else:
            a_f=int(n/2)
            indexs = self.get_subindex(n, y, a_f, ID)
            for ind in indexs:
                video.append(cv2.imread(p + '/' + str(ind) + '.jpg'))
            VV = video
            

        
        mw = 50
        mh = 50
        inputs = torch.rand(self.frame_number, 1, self.win_s, self.win_s)
        #print(len(VV))
        #print(p)
        #crop the central part of each frame to generate input of the network
        for i in range(0, self.frame_number):
            #print(i)
            i1 = int(mw - self.win_s / 2)
            i2 = int(mw + self.win_s / 2)
            j1 = int(mh - self.win_s / 2)
            j2 = int(mh + self.win_s / 2)
            try:
                #print(VV[i].shape)
                tmp_2 = VV[i][i1:i2, j1:j2, :]
            except:
                print('errrrrrrrrrrrrrrrrrrrrrooooooooooooooooooooooooooooooooooorrrrrrrrrrrrrrrrr')
                print(p)
                print(indexs)
                

            img_tensor = self.transform(tmp_2.copy())
            inputs[i, :, :, :] = Variable(img_tensor)

        X = inputs
        return X, y, ID



    """
    This function selects frame indices of the sample with ID=ID
    stack of frames can be selected randomly from each section of the original sample, with step size = self.step_size
    n : length of the sample (number of stored frames)
    cc : label
    activity_frame : index of attack frame
    ID : ID of the sample
    """
    def get_subindex(self, n, cc, activity_frame, ID):
        # l equals (max_index-min_index)
        #print(activity_frame)
        l = int(np.ceil(self.frame_number * self.step_size)) - 1

        if self.phase == 'val':
            idx1 = activity_frame - int(l/2)
            idx2 = idx1 + l
            if idx2 > n:
                d = idx2 - n
                idx1 -= d
                idx2 -= d
        else:
            if ID[0:3] != 'aug':
                idx1 = activity_frame - int(l/2)
                idx2 = idx1 + l
                if idx2 > n:
                    d = idx2 - n
                    idx1 -= d
                    idx2 -= d

            else:
                if cc == 0:
                    l = int(np.ceil(self.frame_number * self.step_size))
                    idx1 = np.random.randint(0, n - l)
                    idx2 = idx1 + l
                    sub_index = []
                    for i in range(idx1, idx2 + 1):
                        sub_index.append(i)
                    return sub_index

                #only for the case of train positive samples, a margin should be kept around the attack frame
                else:
                    while True:
                        l = int(np.ceil(self.frame_number * self.step_size))
                        idx1 = np.random.randint(0, n - l)
                        idx2 = idx1 + l
                        if idx2 > self.right_penlaty + activity_frame and idx1 < activity_frame - self.left_penlaty:
                            sub_index = []
                            for i in range(idx1, idx2 + 1):
                                sub_index.append(i)

                            return sub_index

        sub_index = []
        #print(idx1,idx2,self.step_size)
        for i in range(idx1, idx2, self.step_size):
            sub_index.append(i)

        return sub_index

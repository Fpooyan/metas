import cv2
import os
import numpy as np
import pdb
import torch
import torch.nn as nn
from torchvision import datasets, transforms
import torch.nn.functional as F
import sys
# import torch.nn.init as init
from PIL import Image
sys.path.append("./final_network_clean_with_comments/Network_model/")
from C3D_model_separable import C3D
from torch.autograd import Variable
from torch.nn.parameter import Parameter
import argparse




class DistanceModule(nn.Module):
    def __init__(self, feature_dim, dim_out, class_dim, with_mean, norm_type, cov_type):
        super(DistanceModule, self).__init__()
        self.with_mean = with_mean
        self.class_dim = class_dim

        if cov_type == 'normal':
            self.cov = Parameter(torch.Tensor(dim_out, feature_dim, class_dim))
            if self.with_mean:
                self.mean_params = Parameter(torch.Tensor(class_dim, dim_out))
        elif cov_type == 'equal':
            self.cov = Parameter(torch.Tensor(dim_out, feature_dim))
            if self.with_mean:
                self.mean_params = Parameter(torch.Tensor(class_dim, dim_out))
        elif cov_type == 'identity':
            self.mean_params = Parameter(torch.Tensor(class_dim, feature_dim))

        self.norm_type = norm_type
        self.cov_type = cov_type

    def forward(self, x):

        if self.cov_type == 'normal':
            if self.with_mean:
                h = F.linear(x, self.cov[:, :, 0], bias=self.mean_params[0, :])
            else:
                h = F.linear(x, self.cov[:, :, 0])
        elif self.cov_type == 'equal':
            if self.with_mean:
                h = F.linear(x, self.cov[:, :], bias=self.mean_params[0, :])
            else:
                h = F.linear(x, self.cov[:, :])
        elif self.cov_type == 'identity':
            h = x + self.mean_params[0, :].repeat(x.size(0), 1)

        if self.norm_type == 'l1':
            out = h.abs().sum(1).unsqueeze(1)
        elif self.norm_type == 'l2':
            out = h.pow(2).sum(1).unsqueeze(1)

        for c in range(1, self.class_dim):

            if self.cov_type == 'normal':
                if self.with_mean:
                    h = F.linear(x, self.cov[:, :, c], bias=self.mean_params[c, :])
                else:
                    h = F.linear(x, self.cov[:, :, c])
            elif self.cov_type == 'equal':
                if self.with_mean:
                    h = F.linear(x, self.cov[:, :], bias=self.mean_params[c, :])
                else:
                    h = F.linear(x, self.cov[:, :])
            elif self.cov_type == 'identity':
                h = x + self.mean_params[c, :].repeat(x.size(0), 1)

            if self.norm_type == 'l1':
                out = torch.cat((out, h.abs().sum(1).unsqueeze(1)), 1)
            elif self.norm_type == 'l2':
                out = torch.cat((out, h.pow(2).sum(1).unsqueeze(1)), 1)
        
        return out



class Net(nn.Module):
    def __init__(self,params):
        super(Net, self).__init__()
        self.net = C3D(params['in_depth'] , params['num_features'])
        self.dist_out = DistanceModule(params['num_features'], params['dim_out'], params['num_class'],
                                       params['use_mean'], params['norm'], params['cov'])
        

    def forward(self, x):
        batch_size, timesteps, C, H, W = x.size()
        c_in = x.view( batch_size ,C, timesteps, H, W)
        c_out = self.net(c_in)
        c_out = self.dist_out(c_out)
        
        return c_out


def slid_win(params,dataset_path,model_path,video_name,ext,x_slid_step,y_slid_step,t_slid_step,winsize,numofframes,temporal_step_size,spatial_scale_factor,outpath):
    if os.path.exists(os.path.join(outpath,video_name+'.npy')):
        print("This scoremap is already generated")
        return

    preprocess = transforms.Compose([
        transforms.ToPILImage(),
        transforms.Grayscale(),
        transforms.ToTensor(),
    ])

    model_ft = Net(params)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    use_gpu = torch.cuda.is_available()
    if use_gpu:
        model_ft = model_ft.cuda()
    model_ft.load_state_dict(torch.load(model_path))
    model_ft.train(False)


    with torch.no_grad():
        vid = cv2.VideoCapture(os.path.join(dataset_path,video_name+ext))
        width=vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        height=vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
        width = width / spatial_scale_factor
        height = height / spatial_scale_factor
        dim = (int(width), int(height))
        numframes = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
        #######################
        n_t = int(numframes/t_slid_step)
        n_y = int(height/y_slid_step)
        n_x = int(width/x_slid_step)
        #######################


        Scores = np.zeros([n_t, n_y, n_x])
        vol = torch.rand(1,numofframes*temporal_step_size, 1, int(height), int(width))
        tmp = torch.rand(1, numofframes, 1, int(height), int(width))


        for i_t in range(n_t):
            if i_t == 0:
                for i_f in range(numofframes*temporal_step_size):
                    r,v = vid.read()
                    if r:
                        v_res = cv2.resize(v, dim)
                        img_tensor = preprocess(v_res.copy())
                        vol[:,i_f, :, :, :] = Variable(img_tensor)

            else:
                for i_f in range(numofframes*temporal_step_size):
                    if i_f < numofframes*temporal_step_size-t_slid_step:
                        vol[:,i_f, :,:,:] = vol[:,i_f+t_slid_step, :,:,:]
                    else:
                        r,v = vid.read()
                        if r:
                            v_res = cv2.resize(v, dim)
                            img_tensor = preprocess(v_res.copy())
                            vol[:,i_f, :, :, :] = Variable(img_tensor)
                        else:
                            print('end')

            zz=0
            for z in range(0,numofframes*temporal_step_size,temporal_step_size):
                tmp[:,zz, :, :, :]=vol[:,z, :, :, :]
                zz+=1
            vi=tmp.to(device)

            for i_y in range(n_y-2):
                for i_x in range(n_x-2):
                    input = vi[:,:,:, i_y*y_slid_step:i_y*y_slid_step+winsize, i_x*x_slid_step:i_x*x_slid_step+winsize]
                    output = model_ft(input)
                    Scores[i_t, i_y, i_x] = output[0].item()


        np.save(os.path.join(outpath,video_name+'.npy'), Scores)

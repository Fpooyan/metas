import numpy as np
import os
import cv2


def flag_seen_neighbours(flag, seen, index, x_overlap, y_overlap, t_overlap):

    t, y, x = index
    tr, yr, xr = seen.shape
    s_t = t - t_overlap
    e_t = t + t_overlap + 1
    s_y = y - y_overlap
    e_y = y + y_overlap + 1
    s_x = x - x_overlap
    e_x = x + x_overlap + 1
    if (t - t_overlap < 0):
        s_t = 0
    elif (t + t_overlap > tr - 1):
        e_t = tr
    if (x - x_overlap < 0):
        s_x = 0
    elif (x + x_overlap > xr - 1):
        e_x = xr
    if (y - y_overlap < 0):
        s_y = 0
    elif (y + y_overlap > yr - 1):
        e_y = yr
    flag[s_t:e_t, s_y:e_y, s_x:e_x] = 0
    seen[s_t:e_t, s_y:e_y, s_x:e_x] = np.inf
    flag[t, y, x] = 1


def non_max(video_name,s,suppress_th,x_overlap ,y_overlap ,t_overlap,outpath):
    if os.path.exists(os.path.join(outpath,video_name+'_nms.npy')):
        print("non max is already applied for this score map")
        return
    tc = 0
    s[s==0] = np.inf
    sh = s.shape
    c = 0
    flag = np.ones(s.shape)
    seen = np.ones(s.shape)
    print(s.shape[0]*s.shape[1]*s.shape[2])
    count = 0
    while np.any(seen == 1):
        b = s * seen
        if np.min(b) == np.inf:
            break
        count+=1
        ind = np.unravel_index(np.argmin(b, axis=None), b.shape)
        print(count ,"   ", b[ind])
        flag_seen_neighbours(flag, seen, ind, x_overlap,y_overlap,t_overlap)

    s[np.where(flag == 0)] = np.inf
    np.save(os.path.join(outpath,video_name+'_nms.npy'),s)


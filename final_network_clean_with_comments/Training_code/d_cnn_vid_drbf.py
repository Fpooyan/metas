from __future__ import print_function, division
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
from torchvision import datasets, transforms
import matplotlib.pyplot as plt
import time
import os
import torch.nn.functional as F
import torch.nn.init as init
from PIL import Image
import json
import sys
sys.path.append("/home/metas/fresh_start_codes/metas/final_network_clean_with_comments/Video_loader")

from vid_loader_newaug import Dataset
sys.path.append("/home/metas/fresh_start_codes/metas/final_network_clean_with_comments/Network_model")
from C3D_model_separable import C3D
import cv2

from torch.nn.parameter import Parameter
import argparse


""""
params = {
    'num_class': 1,
    'num_features': fc_num,  # the dimensionality of feature space
    'batch_size': 10,
    'num_epochs': 30,
    'learning_rate': 0.01,
    'cov': 'normal',  # 'normal', 'equal' or 'identity'
    'dim_out': 2,  # the dimensionality of each category's output, needed when covariance type is normal
    'use_mean': True,
    'L2_reg': 0,  # for weight decay
    'version': 'DRBF',  # 'NCC', 'DRBF', 'CNN' or 'LeNet5'
    'norm': 'l2',  # 'l1' or 'l2'
    'rejection': True,  # do you want to reject in the evaluation phase?
    'neg_training': True,  # do the negative examples exist in the training?
    'lambda1': 50,    # max threshold for the positive samples distance
    'lambda2': 500,   # min threshold for the negative samples distance
}
"""


"""
RBF layer distance calculation
"""
class DistanceModule(nn.Module):
    def __init__(self, feature_dim, dim_out, class_dim, with_mean, norm_type, cov_type):
        super(DistanceModule, self).__init__()
        self.with_mean = with_mean
        self.class_dim = class_dim

        if cov_type == 'normal':
            self.cov = Parameter(torch.Tensor(dim_out, feature_dim, class_dim))
            if self.with_mean:
                self.mean_params = Parameter(torch.Tensor(class_dim, dim_out))
        elif cov_type == 'equal':
            self.cov = Parameter(torch.Tensor(dim_out, feature_dim))
            if self.with_mean:
                self.mean_params = Parameter(torch.Tensor(class_dim, dim_out))
        elif cov_type == 'identity':
            self.mean_params = Parameter(torch.Tensor(class_dim, feature_dim))

        self.norm_type = norm_type
        self.cov_type = cov_type

    def forward(self, x):

        if self.cov_type == 'normal':
            if self.with_mean:
                h = F.linear(x, self.cov[:, :, 0], bias=self.mean_params[0, :])
            else:
                h = F.linear(x, self.cov[:, :, 0])
        elif self.cov_type == 'equal':
            if self.with_mean:
                h = F.linear(x, self.cov[:, :], bias=self.mean_params[0, :])
            else:
                h = F.linear(x, self.cov[:, :])
        elif self.cov_type == 'identity':
            h = x + self.mean_params[0, :].repeat(x.size(0), 1)

        if self.norm_type == 'l1':
            out = h.abs().sum(1).unsqueeze(1)
        elif self.norm_type == 'l2':
            #print(h.size())
            out = h.pow(2).sum(1).unsqueeze(1)

        for c in range(1, self.class_dim):

            if self.cov_type == 'normal':
                if self.with_mean:
                    h = F.linear(x, self.cov[:, :, c], bias=self.mean_params[c, :])
                else:
                    h = F.linear(x, self.cov[:, :, c])
            elif self.cov_type == 'equal':
                if self.with_mean:
                    h = F.linear(x, self.cov[:, :], bias=self.mean_params[c, :])
                else:
                    h = F.linear(x, self.cov[:, :])
            elif self.cov_type == 'identity':
                h = x + self.mean_params[c, :].repeat(x.size(0), 1)

            if self.norm_type == 'l1':
                out = torch.cat((out, h.abs().sum(1).unsqueeze(1)), 1)
            elif self.norm_type == 'l2':
                out = torch.cat((out, h.pow(2).sum(1).unsqueeze(1)), 1)
        
        return out



"""
Network structure, consists of C3D and RBF distance module
"""
class Net(nn.Module):
    def __init__(self,params):
        super(Net, self).__init__()
        self.net = C3D(params['in_depth'] , params['num_features'])
        self.dist_out = DistanceModule(params['num_features'], params['dim_out'], params['num_class'],
                                       params['use_mean'], params['norm'], params['cov'])
        

    def forward(self, x):
        batch_size, timesteps, C, H, W = x.size()
        c_in = x.view( batch_size ,C, timesteps, H, W)
        c_out = self.net(c_in)
        #print(c_out.size())
        c_out = self.dist_out(c_out)
        
        return c_out

"""
MLLoss function with two thresholds
"""
class MLLoss(nn.Module):

    def __init__(self, lmb1,lmb2, neg_train):
        super().__init__()
        self.lmbDa1 = lmb1
        self.lmbDa2 = lmb2
        self.neg_train = neg_train
        self.nnl = nn.NLLLoss()

    def forward(self, x, target):

        v_2 = self.lmbDa2 - x
        h_2 = v_2.clamp(min=0)
        h_2 = h_2 + torch.log(torch.exp(-h_2) + torch.exp(v_2 - h_2))  # using log-sum-exp trick
        h2_sum = h_2.sum(1)

        
        v_1 = x - self.lmbDa1
        h_1 = v_1.clamp(min=0)
        h_1 = h_1 + torch.log(torch.exp(-h_1) + torch.exp(v_1 - h_1))  # using log-sum-exp trick
                
        out = torch.cat((-h2_sum.unsqueeze(1),-h_1), 1)
        
        return self.nnl(out, target)

"""
Training loop
"""
def train_model(params,model_save_path,jsons_path):
    print('Training function')

    preprocess = transforms.Compose([
        transforms.ToPILImage(),
        transforms.Grayscale(),
        transforms.ToTensor(),
    ])
    with open(jsons_path+'/data_json.json') as f:
        mydata = json.load(f)
    with open(jsons_path+'/ID_json.json') as f:
        labels = json.load(f)
    with open(jsons_path+'/LEN_json.json') as f:
        length = json.load(f)
    
    print('jsons are loaded...')

    image_datasets = {}
    image_datasets.update({'train': Dataset(mydata['train'], labels, preprocess, length,'train',params)})
    image_datasets.update({'val': Dataset(mydata['val'], labels, preprocess, length,  'val',params)})

    dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size =10,
                                                  shuffle=True, num_workers=0, drop_last=True)
                   for x in ['train', 'val']}
    dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'val']}
    use_gpu = torch.cuda.is_available()

    model = Net(params)
    if use_gpu:
        model = model.cuda()

    model_parameters = list(model.parameters())
    [init.normal_(m) for m in model_parameters]
    model_cov_parameters = list(model.dist_out.parameters())
    [init.normal_(m, std=0.1) for m in model_cov_parameters]

    nnum = sum(p.numel() for p in model.parameters() if p.requires_grad)
    weights = torch.tensor([1 / 4, 3 / 4], dtype=torch.float).cuda()

    criterion = MLLoss(params['lambda1'], params['lambda2'], params['neg_training']).cuda()

    optimizer = optim.Adam(model.parameters(), lr=params['learning_rate'], weight_decay=params['L2_reg'])

    scheduler = lr_scheduler.StepLR(optimizer, step_size=9, gamma=0.1)

    since = time.time()
    count = 0
    best_model_wts = model.state_dict()
    f1_max = 0
    for epoch in range(params['num_epochs']):
        print('Epoch {}/{}'.format(epoch, params['num_epochs'] - 1))
        print('-' * 10)
        fnlist = []
        for phase in ['train', 'val']:
            
            if phase == 'train':
                scheduler.step()
                model.train(True)  # Set model to training mode
                
            else:
                model.train(False)  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            TP = 0
            FP = 0
            TN = 0
            FN = 0

            dataload = dataloaders[phase]
            for data in dataload:
                count+=1
                inputs, labels, ID = data
                #print(inputs.shape)
       

                if use_gpu:
                    inputs = Variable(inputs.cuda())
                    labels = Variable(labels.cuda().long())
                else:
                    inputs, labels = Variable(inputs), Variable(labels.long())

                
                optimizer.zero_grad()
                outputs = model(inputs)                
                
                th_preds = torch.where(outputs[:,0] < params['train_th'],
                                            torch.ones(outputs.size()[0], dtype=torch.int64).cuda(),
                                            torch.zeros(outputs.size()[0], dtype=torch.int64).cuda())



                loss = criterion(outputs, labels)
                
                if phase == 'train':
                    loss.backward()
                    optimizer.step()

                bl = labels.size(0)

                for ii in range(0, bl):
                    if ID[ii][0:3] != 'aug':
                        main_class = labels[ii].item()
                        pred_class = th_preds[ii].item()

                        if main_class == 1:
                            if pred_class == 1:
                                TP += 1
                            else:
                                FN += 1
                        if main_class == 0:
                            if pred_class == 0:
                                TN += 1
                            else:
                                FP += 1

                running_loss += loss.data.item()
                running_corrects += torch.sum(th_preds == labels.data).item()
                
       
                

            dsize =dataset_sizes[phase]
            epoch_loss = running_loss / dsize
            epoch_acc = running_corrects / dsize
            epoch_f1score = TP/(TP+((FP+FN)/2))

            if phase == 'train':
                ff = open(os.path.join(model_save_path,'results','train_accloss.txt'), 'a+')
                ff.write(str(epoch) + ':' +str(epoch_f1score)+','+ str(epoch_acc) + ',' + str(epoch_loss) + '\n')
                ff.close()

                ff = open(os.path.join(model_save_path,'results','train_confmatrix.txt'), 'a+')
                ff.write('epoch ' + str(epoch) + ':' + '\n')
                ff.write(str(TP) + ',' + str(FP) + '\n')
                ff.write(str(FN) + ',' + str(TN) + '\n')
                ff.write('........................' + '\n')
                ff.close()

            elif phase == 'val':
                ff = open(os.path.join(model_save_path,'results','val_accloss.txt'), 'a+')
                ff.write(str(epoch) + ':' +str(epoch_f1score)+','+ str(epoch_acc) + ',' + str(epoch_loss) + '\n')
                ff.close()

                ff = open(os.path.join(model_save_path,'results','val_confmatrix.txt'), 'a+')
                ff.write('epoch ' + str(epoch) + ':' + '\n')
                ff.write(str(TP) + ',' + str(FP) + '\n')
                ff.write(str(FN) + ',' + str(TN) + '\n')
                ff.write('........................' + '\n')
                ff.close()

            if epoch_f1score>f1_max and phase=='val':
                f1_max=epoch_f1score
                torch.save(model.state_dict(), os.path.join(model_save_path, 'model_params.pt'))
                torch.save(model,os.path.join(model_save_path, 'model.pt') )


    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))

    model.load_state_dict(best_model_wts)
    return model

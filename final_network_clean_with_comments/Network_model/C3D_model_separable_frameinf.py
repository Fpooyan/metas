import torch
import torch.nn as nn


class C3D(nn.Module):
    """
    The C3D network.

    SeperableConv3d : custom block implementing depth_time separable  3D-convolutional filters
    """

    def SeperableConv3d(self, in_channels, out_channels, kernel_size_spatial, kernel_size_temporal, stride_spatial, stride_temporal):

        return nn.Sequential(
            nn.Conv3d(in_channels=in_channels, out_channels=in_channels, kernel_size=(1,kernel_size_spatial,kernel_size_spatial),
                   groups=in_channels, stride=(stride_temporal,stride_spatial,stride_spatial)),
            nn.ELU(),
            nn.Conv3d(in_channels=in_channels, out_channels=in_channels, kernel_size=(kernel_size_temporal,1,1), groups=in_channels,
                   stride=(stride_temporal,1,1)),
            nn.ELU(),
            nn.Conv3d(in_channels=in_channels, out_channels=out_channels, kernel_size=(1,1,1))
        )
    def __init__(self,in_depth, fc_num):
        super(C3D, self).__init__()

        self.R1=nn.LeakyReLU()
        self.R=nn.ELU()

        self.convs1 = self.SeperableConv3d(1, in_depth, 3, 3, 2, 1)
        self.BN=nn.BatchNorm3d(in_depth)

        self.convs2 = self.SeperableConv3d(in_depth, in_depth*2, 3, 3, 1, 1)
        self.convs2_a = self.SeperableConv3d(in_depth*2, in_depth*2, 3, 3, 1, 1)
        self.BN2=nn.BatchNorm3d(in_depth*2)
        self.BN2a=nn.BatchNorm3d(in_depth*2)

        
        self.convs3 = self.SeperableConv3d(in_depth*2, in_depth*4, 3, 3, 1, 1)
        self.convs3_a = self.SeperableConv3d(in_depth*4, in_depth*4, 3, 3, 1, 1)
        self.BN3=nn.BatchNorm3d(in_depth*4)
        self.BN3a=nn.BatchNorm3d(in_depth*4)

        self.convs4 = self.SeperableConv3d(in_depth*4, in_depth*4, 3, 3, 1, 1)
        self.convs4_a = self.SeperableConv3d(in_depth*4, in_depth*4, 3, 3, 1, 1)
        self.BN4=nn.BatchNorm3d(in_depth*4)
        self.BN4a=nn.BatchNorm3d(in_depth*4)
        
        self.pool = nn.MaxPool3d(kernel_size=(1,7 ,7))
        self.fc2conv = nn.Conv3d(in_depth*4,fc_num,kernel_size = (1,1,1))
        self.__init_weight()


    


    def forward(self, x):

        
        x = self.convs1(x)
        x = self.R(self.BN(x))
        

        x = self.convs2(x)
        x = self.R(self.BN2(x))
        
        x = self.convs2_a(x)
        x = self.R(self.BN2a(x))

        x = self.convs3(x)
        x = self.R(self.BN3(x))
        
        x = self.convs3_a(x)
        x = self.R(self.BN3a(x))
        

        x = self.convs4(x)
        x = self.R(self.BN4(x))
        
        x = self.convs4_a(x)
        x = self.R(self.BN4a(x))
        
        x = self.pool(x)
        
     
        ss=x.size();

        x = self.fc2conv(x)
        #print(x.size())
        #x = x.view(1,-1)
        ss = x.size()
        #x = x.view(ss[1],ss[3],ss[4])
        x = torch.transpose(x,1,4)
        x = torch.transpose(x,1,3)
        ss = x.size()
        #print(ss)
        #x = x.reshape(ss[1]*ss[3],-1)
        #print(x.size())
        x = x.view(ss[1]*ss[3],ss[4])
        

        return x,ss


    def __init_weight(self):
        for m in self.modules():
            if isinstance(m, nn.Conv3d):
                # n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                # m.weight.data.normal_(0, math.sqrt(2. / n))
                torch.nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, nn.BatchNorm3d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()




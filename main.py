import json
import numpy as np
import os
import sys
from shutil import copy
sys.path.append("./datasetcodes/")

from random_neg_labeler import save_neg_centers
from extractor import extractor
from pos_locator import pos_locator
from false_pos_labeler import save_false_pos_centers
sys.path.append("./final_network_clean_with_comments/Video_loader/")

from create_json_train import create_jsons

from slid_win_test import slid_win
from nonmax import non_max
sys.path.append("./final_network_clean_with_comments/Training_code/")

from d_cnn_vid_drbf import train_model

def main(path,experiment_number):



    params={

        'in_depth' : 32,

        'num_class': 1,

        'num_features': 512,  # the dimensionality of feature space, fc_num

        'batch_size': 10,

        'num_epochs': 30,

        'learning_rate': 0.01,

        'cov': 'normal',  # 'normal', 'equal' or 'identity'

        'dim_out': 2,  # the dimensionality of each category's output, needed when covariance type is normal

        'use_mean': True,

        'L2_reg': 0,  # for weight decay

        'version': 'DRBF',  # 'NCC', 'DRBF', 'CNN' or 'LeNet5'

        'norm': 'l2',  # 'l1' or 'l2'

        'rejection': True,  # do you want to reject in the evaluation phase?

        'neg_training': True,  # do the negative examples exist in the training?

        'lambda1': 50,  # max threshold for the positive samples distance

        'lambda2': 500,  # min threshold for the negative samples distance





        'stages' : 1, #the number of stages to run

        'pos_path': None, #"/hdd2/metas_proj/metas/fresh_start/new_attack_jsons", #path to positive centers to use in the starting stage if available, or None otherwise



        'dataset_path': "/hdd2/metas_proj/videos_outputs/checked", #path to dataset

        'winsize':40, #training window size

        'numofframes':15, #training number of frames

        'step_size':2, #step size for frame sampling



        'rand_neg_num':500, #number of random neg samples generated per video



        'x_slid_step':15, #sliding step on x axis for sliding window

        'y_slid_step':15, #sliding step on y axis for sliding window

        't_slid_step':10, #sliding step on t axis for sliding window

        'slidwin_step_size':2, #step size for frame sampling in sliding window

        'spatial_scale_factor':1, #scale factor applied to frame size before applying sliding window



        'suppress_th':1000, #suppression threshold in non max

        'nms_x_overlap':2, #non max x axis overlap

        'nms_y_overlap': 2, #non max y axis overlap

        'nms_t_overlap': 2, #non max t axis overlap



        'pos_locator_th':375, # threshold for choosing positive samples from score map



        'spatial_overlap':0, #false positive samples spatial overlap threshold

        'temporal_overlap':0, #false positive samples temporal overlap threshold



        'ex_step_size':2, #extraction step size for frame sampling

        'pos_ex_winsize':100, #extraction window size for pos sample

        'neg_ex_winsize': 100, #extraction window size for neg sample

        'pos_ex_numofframes': 100, #extration numofframes for pos samples

        'neg_ex_numofframes': 100, #extraction numofframes for neg samples



        'vals':["48","52","57","59","64","67","68","21"], #list of vals for json generation

        'healthy_percents':[100,40], #list of precentage of each healthy folder for json generation

        'n_aug_train_healthy':[0,0], #list of number of aug per sample for each healthy folder



        'train_th':375, #threshold for calculating conf matrix while training



    }



    for st in range(params['stages']):



        #Search for the last completed stage
        
        if os.path.exists(os.path.join(path,'models')):

            current_stage=len(os.listdir(os.path.join(path,'models')))

        else:

            os.mkdir(os.path.join(path,'models'))

            current_stage=0

        """

        Making tree of directories for current stage

        """

        #make dirs for the current stage

        current_stage_path = os.path.join(path,'stage_'+str(current_stage))
        pos_centers_path = os.path.join(current_stage_path, 'centers','Pos')
        neg_centers_path = os.path.join(current_stage_path, 'centers','Neg')
        pos_ex_path = os.path.join(current_stage_path, 'extracted','Pos')
        neg_ex_path = os.path.join(current_stage_path, 'extracted', 'Neg')
        if not os.path.exists(current_stage_path):
            os.mkdir(current_stage_path)

            os.mkdir(os.path.join(current_stage_path, 'centers'))

            os.mkdir(pos_centers_path)

            os.mkdir(neg_centers_path)

            os.mkdir(os.path.join(current_stage_path, 'extracted'))

            os.mkdir(pos_ex_path)

            os.mkdir(neg_ex_path)

            os.mkdir(os.path.join(current_stage_path, 'dataset_jsons'))

        """

        Center files Generation

        """

        #copy positive centers if provided

        if params['pos_path'] is not None and len(os.listdir(pos_centers_path))==0:

            for fl in os.listdir(params['pos_path']):

                if os.path.isfile(os.path.join(params['pos_path'],fl)):

                    copy(os.path.join(params['pos_path'],fl), pos_centers_path)



        #generate negative centers

        if current_stage==0:

            for video_name_full in os.listdir(params['dataset_path']):

                #print(video_name_full)
                video_name, ext = os.path.splitext(video_name_full)

                save_neg_centers(path, current_stage,params['dataset_path'], video_name,ext, params['neg_ex_winsize'], params['neg_ex_numofframes'], params['rand_neg_num'], params['ex_step_size'], neg_centers_path)

        else:

            model_path = os.path.join(path,'models','stage_'+str(current_stage-1),'model_params.pt')
            if not os.path.exists(os.path.join(path,'models','stage_'+str(current_stage-1),'scores')):
                os.mkdir(os.path.join(path,'models','stage_'+str(current_stage-1),'scores'))
            for video_name_full in os.listdir(params['dataset_path']):
                
                video_name, ext = os.path.splitext(video_name_full)
                print(video_name)
                slid_win(params,params['dataset_path'], model_path, video_name,ext, params['x_slid_step'], params['y_slid_step'], params['t_slid_step'], params['winsize'], params['numofframes'],

                     params['slidwin_step_size'], params['spatial_scale_factor'],os.path.join(path,'models','stage_'+str(current_stage-1),'scores') )

                print("slid win done")
                scoremap=np.load(os.path.join(path,'models','stage_'+str(current_stage-1),'scores',video_name+'.npy'))

                non_max(video_name, scoremap, params['suppress_th'], params['nms_x_overlap'], params['nms_y_overlap'], params['nms_t_overlap'], os.path.join(path,'models','stage_'+str(current_stage-1),'scores'))
                print("non max done")
                scoremap_nms = np.load(

                    os.path.join(path, 'models', 'stage_' + str(current_stage - 1), 'scores', video_name + '_nms.npy'))

                pos_centers = pos_locator(scoremap_nms,[params['winsize'],params['winsize'],params['numofframes']*params['slidwin_step_size']], params['x_slid_step'], params['y_slid_step'], params['t_slid_step'], params['pos_locator_th'])
                print("pos locate done")
                save_false_pos_centers(path,params['dataset_path'],current_stage, video_name,ext, params['winsize'], params['numofframes'],params['slidwin_step_size'], params['spatial_overlap'],params['temporal_overlap'], pos_centers,neg_centers_path)
                print("fp centers saved")
                

        """

        #Extract training samples

        """

        for video_name_full in os.listdir(os.path.join(params['dataset_path'])):

            video_name, ext = os.path.splitext(video_name_full)
            
            if os.path.exists(os.path.join(pos_centers_path, video_name + "_attack.json")):
                

                extractor(params['dataset_path'], video_name,ext,

                  os.path.join(pos_centers_path, video_name + "_attack.json"),

                  params['pos_ex_winsize'], params['pos_ex_numofframes'], params['ex_step_size'], pos_ex_path)

            
            if current_stage==0 and os.path.exists(os.path.join(neg_centers_path, video_name + "_negative_centers.json")):

                extractor(params['dataset_path'], video_name,ext,

                  os.path.join(neg_centers_path, video_name + "_negative_centers.json"),

                  params['neg_ex_winsize'], params['neg_ex_numofframes'], params['ex_step_size'], neg_ex_path)

            elif os.path.exists(os.path.join(neg_centers_path, video_name + "_fp_centers.json")):

                print("fp extraction")
                extractor(params['dataset_path'], video_name,ext,

                          os.path.join(neg_centers_path, video_name + "_fp_centers.json"),

                          params['neg_ex_winsize'], params['neg_ex_numofframes'], params['ex_step_size'], neg_ex_path)


        
        """

        #Create train jsons

        """
        
        create_jsons(params['vals'],params['healthy_percents'],params['n_aug_train_healthy'],path)
        print('jsons is created!!!!!!')

        """

        #Train

        """

        os.mkdir(os.path.join(path,'models','stage_'+str(current_stage)))

        os.mkdir(os.path.join(path,'models','stage_'+str(current_stage),'results'))

        train_model(params,os.path.join(path,'models','stage_'+str(current_stage)),os.path.join(path,'stage_'+str(current_stage),'dataset_jsons'))

main('/data/metas/test_experiment',0)


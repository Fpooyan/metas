from tkinter import *

class my_Buttonbox(object):
    def __init__(self,parent):

        self.master = Toplevel(parent)
        lableframe=Frame(self.master )
        lableframe.grid(row=0, column=0)
        Bframe=Frame(self.master )
        Bframe.grid(row=1, column=0)
        self.label=Label(lableframe, text="Choose the correct label? (if you close this window, Nothing will be saved)")
        self.label.pack(pady=20,padx=10)
        self.b0 = Button(Bframe, text="Healthy",command=self.e0)
        self.b0.grid(row=0, column=0,pady=50)
        self.b1 = Button(Bframe, text="First type",command=self.e1)
        self.b1.grid(row=0, column=1,padx=10)
        self.b2 = Button(Bframe, text="Second type",command=self.e2)
        self.b2.grid(row=0, column=2,padx=10)
        self.b3 = Button(Bframe, text="Unknown",command=self.e3)
        self.b3.grid(row=0, column=3,padx=10)
        self.master.deiconify()
        self.master.wait_window()
        #self.master.protocol("WM_DELETE_WINDOW", self.on_closing)
        #self.master.mainloop()
    def e0(self):
        self.label=0
        self.master.destroy()
    def e1(self):
        self.label=1
        self.master.destroy()
    def e2(self):
        self.label=2
        self.master.destroy()
    def e3(self):
        self.label=3
        self.master.destroy()

    #def on_closing(self):
        #self.label=None
        #self.master.destroy()

import tkinter
import cv2
import PIL.Image, PIL.ImageTk
import time
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import os
import easygui as eg
import shutil
from B_box import my_Buttonbox

def compute_resize_factor(h,w,win_h,win_w):
  rf=(0.85*win_h)/h
  while True:
    if rf*w<(0.73*win_w):
        print(rf)
        return rf
    rf-=0.05


# The factory function
def dnd_start(source, event):
    h = DndHandler(source, event)
    if h.root:
        return h
    else:
        return None


# The class that does the work

class DndHandler:

    root = None

    def __init__(self, source, event):
        if event.num > 5:
            return
        root = event.widget._root()
        try:
            root.__dnd
            return # Don't start recursive dnd
        except AttributeError:
            root.__dnd = self
            self.root = root
        self.source = source
        self.target = None
        self.initial_button = button = event.num
        self.initial_widget = widget = event.widget
        self.release_pattern = "<B%d-ButtonRelease-%d>" % (button, button)
        self.save_cursor = widget['cursor'] or ""
        widget.bind(self.release_pattern, self.on_release)
        widget.bind("<Motion>", self.on_motion)
        widget['cursor'] = "hand2"

    def __del__(self):
        root = self.root
        self.root = None
        if root:
            try:
                del root.__dnd

            except AttributeError:
                pass

    def on_motion(self, event):
        x, y = event.x_root, event.y_root
        target_widget = self.initial_widget.winfo_containing(x, y)
        source = self.source
        new_target = None
        while target_widget:
            try:
                attr = target_widget.dnd_accept
            except AttributeError:
                pass
            else:
                new_target = attr(source, event)
                if new_target:
                    break
            target_widget = target_widget.master
        old_target = self.target
        if old_target is new_target:
            if old_target:
                old_target.dnd_motion(source, event)
        else:
            if old_target:
                self.target = None
                old_target.dnd_leave(source, event)
            if new_target:
                new_target.dnd_enter(source, event)
                self.target = new_target

    def on_release(self, event):
        self.finish(event, 1)

    def cancel(self, event=None):
        self.finish(event, 0)

    def finish(self, event, commit=0):
        target = self.target
        source = self.source
        widget = self.initial_widget
        root = self.root
        try:
            del root.__dnd
            self.initial_widget.unbind(self.release_pattern)
            self.initial_widget.unbind("<Motion>")
            widget['cursor'] = self.save_cursor
            self.target = self.source = self.initial_widget = self.root = None
            if target:
                if commit:
                    target.dnd_commit(source, event)
                else:
                    target.dnd_leave(source, event)
        finally:
            source.dnd_end(target, event)



# ----------------------------------------------------------------------
# The rest is here for testing and demonstration purposes only!

class Icon:

    def __init__(self, name,type=-1,prev=False):
        self.name = name
        self.canvas = self.label = self.id = None
        self.prev=prev
        self.type=type

    def attach(self, canvas, x=100, y=100,w_s=100):

        if canvas is self.canvas:
            self.canvas.coords(self.id, x, y)
            return
        if self.canvas:
            self.detach()
        if not canvas:
            return
        txt=self.name
        ww=5
        hh=1
        ccolor='red'
        if self.prev:
            txt='type '+self.type
            ww=5
            hh=2
            ccolor='blue'
        label = tkinter.Label(canvas, text=txt,
                              borderwidth=0,bg=ccolor,cursor="hand1",height=hh, width=ww)

        id = canvas.create_window(x, y-w_s, window=label, anchor="nw")
        self.canvas = canvas
        self.label = label
        self.id = id
        label.bind("<ButtonPress>", self.press)


    def detach(self):
        canvas = self.canvas
        if not canvas:
            return
        id = self.id
        label = self.label
        self.canvas = self.label = self.id = None
        canvas.delete(id)
        label.destroy()

    def press(self, event):
        if dnd_start(self, event):
            # where the pointer is relative to the label widget:
            self.x_off = event.x
            self.y_off = event.y
            # where the widget is relative to the canvas:
            self.x_orig, self.y_orig = self.canvas.coords(self.id)

    def putback(self):
        self.canvas.coords(self.id, self.x_orig, self.y_orig)

    def where(self, canvas, event,frame_number,center_files,name_dict,flag,rf,w_s=100):
        # where the corner of the canvas is relative to the screen:
        x_org = canvas.winfo_rootx()
        y_org = canvas.winfo_rooty()
        # where the pointer is relative to the canvas widget:
        x = event.x_root - x_org
        y = event.y_root - y_org
        cell_x=x
        cell_y=y+w_s
        if flag:
            k=name_dict[self.name]
            if k>=0:
                center_files[k].write(str(int(cell_x/rf)) + ',' + str(int(cell_y/rf)) + ',' + str(frame_number) + '\n')
        #print(str(x) + ',' + str(y) + ',' + str(frame_number) + '\n')
        # compensate for initial pointer offset
        return x - self.x_off, y - self.y_off,cell_x,cell_y

    def dnd_end(self, target, event):
        pass
class App1:
 def __init__(self, window, window_title):
     self.window = window
     self.window.title(window_title)
     self.widthpixels = self.window.winfo_screenwidth()
     self.heightpixels = self.window.winfo_screenheight()
     print(self.heightpixels,  self.widthpixels)
     self.resize_factor = None
     self.window.geometry('{}x{}'.format(self.widthpixels, self.heightpixels))
     self.button_frame = Frame(self.window)
     self.button_frame.pack(side=BOTTOM, fill=Y)
     self.button_frame2 = Frame(self.window)
     self.button_frame2.pack(side=LEFT, fill=Y)
     self.img_frame = Frame(self.window)
     self.img_frame.pack(anchor=tkinter.CENTER, expand=True)
     labelframe1 = LabelFrame(self.button_frame, text='')
     labelframe1.pack(fill="both", expand="yes")
     self.labelframe3 = LabelFrame(self.window, text='')
     self.labelframe3.pack(side=BOTTOM, fill=Y)
     labelframe2 = LabelFrame(self.button_frame2, text='Working panel')
     labelframe2.pack(fill="both", expand="yes")
     self.choose_button = Button(labelframe2, text='import video', height=2, width=10, command=self.select)
     self.choose_button.grid(row=0, column=0)
     self.start_button = Button(labelframe2, text='start tracking', height=2, width=10, command=self.start)
     self.start_button.grid(row=1, column=0)
     self.start_button.config(state="disabled")
     self.pause_button = Button(labelframe1, text='pause', height=1, width=10, command=self.pause)
     self.pause_button.grid(row=0, column=1)
     self.pause_button.config(state="disabled")
     self.resume_button = Button(labelframe1, text='resume', height=1, width=10,command=self.resume)
     self.resume_button.grid(row=0, column=2)
     self.resume_button.config(state="disabled")
     self.back_button = Button(labelframe1, text='back', height=1, width=10,command=self.back)
     self.back_button.grid(row=0, column=0)
     self.back_button.config(state="disabled")
     self.up_button = Button(labelframe1, text='speed up', height=1, width=10,command=self.speed_up)
     self.up_button.grid(row=0, column=4)
     self.up_button.config(state="disabled")
     self.down_button = Button(labelframe1, text='speed down', height=1, width=10,command=self.speed_down)
     self.down_button.grid(row=0, column=3)
     self.down_button.config(state="disabled")
     self.cancel_button = Button(labelframe2, text='cancel', height=2, width=10,command=self.cancel)
     self.cancel_button.grid(row=3, column=0)
     self.cancel_button.config(state="disabled")
     self.addt_button = Button(labelframe2, text='Add tracker', height=2, width=10,cursor="plus",command=self.add_tracker)
     self.addt_button.grid(row=2, column=0)
     self.addt_button.config(state="disabled")
     temp = ttk.Separator(labelframe2, orient=HORIZONTAL)
     temp.grid(row=4, column=0, pady=10, sticky="ew")
     self.wsize_label=Label(labelframe2,text='Window Size: 100')
     self.wsize_label.grid(row=5, column=0,pady=10)
     size_frame=Frame(labelframe2)
     size_frame.grid(row=6, column=0)
     self.up_size = Button(size_frame, text='+', height=2, width=5,command=self.up_size)
     self.up_size.grid(row=0, column=0)
     self.down_size = Button(size_frame, text='-', height=2, width=5,command=self.down_size)
     self.down_size.grid(row=0, column=1)
     self.var = IntVar()
     self.def_size=Checkbutton(labelframe2, text="set as default size",variable=self.var,command=self.change_size)
     self.def_size.grid(row=7, column=0)
     temp2 = ttk.Separator(labelframe2, orient=HORIZONTAL)
     temp2.grid(row=8, column=0, pady=10, sticky="ew")
     self.help_button = Button(labelframe2, text='help', height=2, width=10,command=self.my_help)
     self.help_button.grid(row=9, column=0)
     self.video_selected=False
     self.tracker_num=0
     self.Icons=[]
     self.bar_labels = []
     self.cell_folders=[]
     self.is_tagged=[]
     self.is_deleted = []
     self.prev_cells_name=[]
     self.prev_cells_label = []
     self.prev_cells_center = []
     self.prev_labels = []
     self.add_selected=False
     self.vid=None
     if os.path.exists('window_size.txt'):
         with open('window_size.txt', "r")  as ins2:
             lines = ins2.read().splitlines()
             self.p_size=int(lines[0])
             ss = 'Window Size: ' + str(self.p_size)
             self.wsize_label.config(text=ss)

     else:
        self.p_size=100
     self.window.protocol("WM_DELETE_WINDOW", self.on_closing)
 def my_help(self):
     self.helpmaster = Toplevel(self.window)
     self.helpmaster.title('help')
     h1=Label(self.helpmaster,text='         Hi         ')
     h1.grid(row=0, column=0,pady=10)
     h3=Label(self.helpmaster,text='type 0: Healthy cell ')
     h3.grid(row=1, column=0,padx=100)
     h4=Label(self.helpmaster,text='type 1: First attack ')
     h4.grid(row=2, column=0)
     h5=Label(self.helpmaster,text='type 2: Second attack ')
     h5.grid(row=3, column=0)
     h6=Label(self.helpmaster,text='type 3: Unknown ')
     h6.grid(row=4, column=0)
     h2=Label(self.helpmaster,text='Blue: previously labeld')
     h2.grid(row=5, column=0)
     h7=Label(self.helpmaster,text='Red: New cell')
     h7.grid(row=6, column=0)
     BH=Button(self.helpmaster,text='OK',height=2, width=10,command=self.close_help)
     BH.grid(row=7, column=0,pady=10)

 def close_help(self):
     self.helpmaster.destroy()
 def change_size(self):
     #print(self.var.get())
     if self.var.get():
         ff=open('window_size.txt','w')
         ff.write(str(self.p_size)+'\n')
         ff.close()
 def up_size(self):
     self.p_size+=5
     ss='Window Size: '+str(self.p_size)
     self.wsize_label.config(text=ss)
     print(self.tracker_num)
     for i in range(0,self.tracker_num):
         self.Icons[i].attach(self.canvas, self.cell_mid[i][0], self.cell_mid[i][1]-self.p_size-5, self.p_size)
     for i in range(0, len(self.prev_cell_mid)):
         self.prev_Icons[i].attach(self.canvas, self.prev_cell_mid[i][0], self.prev_cell_mid[i][1]-self.p_size-5, self.p_size)


 def down_size(self):
     self.p_size-=5
     ss='Window Size: '+str(self.p_size)
     self.wsize_label.config(text=ss)
     print(self.tracker_num)
     for i in range(0,self.tracker_num):
         self.Icons[i].attach(self.canvas, self.cell_mid[i][0], self.cell_mid[i][1]-self.p_size-5, self.p_size)
     for i in range(0, len(self.prev_cell_mid)):
         self.prev_Icons[i].attach(self.canvas, self.prev_cell_mid[i][0], self.prev_cell_mid[i][1]-self.p_size-5, self.p_size)

 def get_frame(self):
     if self.vid.isOpened():
         ret, frame = self.vid.read()
         if ret:
             img=cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
             img1=cv2.resize(img,(0,0),fx=self.resize_factor,fy=self.resize_factor)
             return (ret,img1 )
         else:
             return (ret, None)
     else:
         return (False, None)
 def speed_down(self):
     self.delay+=20
 def speed_up(self):
     if self.delay>20:
         self.delay-=20
 def add_tracker(self):
     self.window.config(cursor='plus')
     self.add_selected = True
 def add_tracker2(self,xx,yy):
     self.tracker_num+=1
     k=self.tracker_num-1
     subdirs = [o for o in os.listdir(self.video_folder) if
                os.path.isdir(os.path.join(self.video_folder, o))]
     k1=1
     while True:
         if (str(k1) in subdirs)==False:
             tracker_name=str(k1)
             break
         k1+=1
     self.name_dict.update({tracker_name:k})
     self.Icons.append(Icon(tracker_name))
     self.Icons[k].attach(self.canvas,xx,yy,self.p_size)
     self.Icons[k].label.bind("<Button-3>", lambda event, a=k: self.right_click(event,a))
     self.canvas.create_rectangle(xx-self.p_size,yy-self.p_size,xx+self.p_size,yy+self.p_size)
     self.canvas.create_oval(xx-1,yy-1,xx+1,yy+1)
     self.cell_mid.append([xx,yy])
     self.rect_xy.append([xx-self.p_size,yy-self.p_size,xx+self.p_size,yy+self.p_size])
     self.cell_folders.append(self.video_folder + '/' + tracker_name)
     os.makedirs(self.cell_folders[k])
     self.center_files.append(open(self.cell_folders[k] + '/centers.txt', 'w'))
     self.label_files.append(open(self.cell_folders[k] + '/label.txt', 'w'))
     self.activity_files.append(open(self.cell_folders[k] + '/activity.txt', 'w'))
     self.center_files[k].write(str(int(xx/self.resize_factor)) + ',' + str(int(yy/self.resize_factor)) + ',' + str(self.frame_counter) + '\n')
     self.is_tagged.append(False)
     self.is_deleted.append(False)

 def right_click(self,event,k):
     self.pause()
     self.menu = Menu(self.canvas, tearoff=0)
     self.menu.add_radiobutton(label='label and save',command=lambda a=k: self.tag(k))
     self.menu.add_radiobutton(label='add activity', command=lambda a=k: self.activity(k))
     self.menu.add_radiobutton(label='add comment', command=lambda a=k: self.comment(k))
     self.menu.add_radiobutton(label='delete', command=lambda a=k: self.delete_cell(k))
     self.is_right=True
     self.menu.post(event.x_root, event.y_root)
 def prev_right_click(self,event,k):
     self.pause()
     self.menu = Menu(self.canvas, tearoff=0)
     self.menu.add_radiobutton(label='change label',command=lambda a=k: self.prev_tag(k))
     self.menu.add_radiobutton(label='add comment', command=lambda a=k: self.prev_comment(k))
     self.menu.add_radiobutton(label='add Activity', command=lambda a=k: self.prev_activity(k))
     self.is_right=True
     self.menu.post(event.x_root, event.y_root)

 def comment(self,cell_num):
     k = cell_num
     msg = "Enter your comment"
     title = "Add comment"
     fieldNames = ["Comment"]
     fieldValues = eg.multenterbox(msg, title, fieldNames)
     if fieldValues!=None:
         comment_file = open(self.cell_folders[k] + '/comment.txt', 'w')
         comment_file.write(fieldValues[0]+'\n')
         comment_file.close()
     self.resume()
 def prev_comment(self,cell_num):
     k = cell_num
     msg = "Enter your comment"
     title = "Add comment"
     fieldNames = ["Comment"]
     fieldValues = eg.multenterbox(msg, title, fieldNames)
     if fieldValues!=None:
         comment_file = open(self.prev_cell_folders[k] + '/comment.txt', 'w')
         comment_file.write(fieldValues[0]+'\n')
         comment_file.close()
     self.resume()
 def prev_activity(self,cell_num):
     k = cell_num
     activity_file = open(self.prev_cell_folders[k] + '/activity.txt', 'w')
     activity_file.write(str(self.frame_counter) + '\n')
     activity_file.close()
     self.resume()

 def delete_cell(self,cell_num):
     k=cell_num
     self.is_deleted[k]=True
     self.center_files[k].close()
     self.label_files[k].close()
     self.activity_files[k].close()
     shutil.rmtree(self.cell_folders[k])
     self.Icons[k].label.destroy()
     self.rect_xy[k] = None
     self.resume()
 def activity(self,cell_num):
     k=cell_num
     self.activity_files[k].write(str(int(self.cell_mid[k][0]/self.resize_factor)) + ',' + str(int(self.cell_mid[k][1]/self.resize_factor)) + ',' + str(self.frame_counter) + '\n')
     self.resume()
 def load_prev_cells(self):
     subdirs = [o for o in os.listdir(self.video_folder) if
                os.path.isdir(os.path.join(self.video_folder, o))]
     k=0
     for dir in subdirs:

         label_file=self.video_folder+'/'+dir+'/'+'label.txt'
         center_file=self.video_folder+'/'+dir+'/'+'centers.txt'
         with open(label_file) as f:
             label = f.readlines()
         if len(label)>0:
             self.prev_cells_name.append(dir)
             self.prev_cells_label.append(label[0])
             with open(center_file) as f:
                 pcenters = f.readlines()
             pc=[]
             for i in range(0,len(pcenters)):
                 pp=pcenters[i].split(',')
                 pc.append([int(pp[0]),int(pp[1]),int(pp[2])])
             self.prev_cells_center.append(pc)
             xx=int(pc[0][0]*self.resize_factor)
             yy=int(pc[0][1]*self.resize_factor)
             print(xx)
             print(yy)
             self.canvas.create_rectangle(xx - self.p_size, yy - self.p_size, xx + self.p_size, yy + self.p_size)
             self.canvas.create_oval(xx - 1, yy - 1, xx + 1, yy + 1)
             self.prev_cell_mid.append([xx, yy])
             self.name_dict.update({dir: -1})
             self.prev_name_dict.update({dir: k})
             self.prev_Icons.append(Icon(dir,label[0],True))
             self.prev_Icons[k].attach(self.canvas, xx, yy, self.p_size)
             self.prev_Icons[k].label.bind("<Button-3>", lambda event, a=k: self.prev_right_click(event, a))
             self.prev_cell_folders.append(self.video_folder + '/' + dir)
             self.prev_rect_xy.append(True)
             k=k+1
         else:
             shutil.rmtree(self.video_folder+'/'+dir)





 def start(self):

     self.start_button.config(state="disabled")
     self.choose_button.config(state="disabled")
     for child in self.img_frame.winfo_children():
         child.destroy()
     self.close_all()
     self.video_selected = True
     self.tracker_num=0
     self.Icons=[]
     self.prev_Icons = []
     self.cell_folders = []
     self.prev_cell_folders = []
     self.is_tagged = []
     self.is_deleted = []
     self.center_files=[]
     self.activity_files = []
     self.label_files=[]
     self.rect_xy=[]
     self.prev_rect_xy = []
     self.cell_mid=[]
     self.prev_cell_mid = []
     self.name_dict={}
     self.prev_name_dict = {}
     self.prev_cells_name=[]
     self.prev_cells_label = []
     self.prev_cells_center = []
     self.add_selected = False
     self.after_back = False
     self.is_right=False
     self.bar_numbr=None
     self.bar_clicked=False
     #self.window_size=100
     self.pause_button.config(state="normal")
     self.back_button.config(state="normal")
     self.resume_button.config(state="normal")
     self.cancel_button.config(state="normal")
     self.addt_button.config(state="normal")
     self.up_button.config(state="normal")
     self.down_button.config(state="normal")
     self.vid = cv2.VideoCapture(self.video_source)
     if not self.vid.isOpened():
         raise ValueError("Unable to open video source", self.video_source)
     self.vid_length = int(self.vid.get(cv2.CAP_PROP_FRAME_COUNT))
     self.step=20
     self.labels_num=int(self.vid_length /self.step)
     if self.labels_num>110:
         self.labels_num=110
         self.step=int(self.vid_length / self.labels_num)
     self.bar_labels=[]
     for i in range(0, self.labels_num):
         self.bar_labels.append(Label(self.labelframe3,bg='white', borderwidth=0, height=1, width=1))
         self.bar_labels[i].pack(side=LEFT)
         self.bar_labels[i].bind("<Button-1>", lambda event, ai=i: self.click_bar(event, ai))
     self.backk=self.step
     self.prev_label=0
     if os.path.exists('output') == False:
         os.makedirs('output')
     out_name = self.video_source.split('/')[-1].split('.')[0]
     if os.path.exists('output/' + out_name) == False:
         os.makedirs('output/' + out_name)
     self.video_folder='output/' + out_name
     self.mid_x = 0
     self.mid_y = 0
     self.frames = []
     self.frame_counter = -1
     self.back_counter = 0
     self.back_flag = False
     self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
     self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
     print(self.height, self.width)
     self.width=int(self.width*self.resize_factor)
     self.height=int(self.height *self.resize_factor)
     print(self.height,self.width)
     self.canvas = tkinter.Canvas(self.img_frame, width=self.width, height=self.height)
     self.canvas.pack()
     self.canvas.bind('<Button-1>', self.click)
     self.canvas.dnd_accept = self.dnd_accept
     self.is_paused = False
     self.is_back = False
     self.delay = 25
     self.load_prev_cells()
     self.update()
     #threading.Thread(target=self.update).start()

 def click_bar(self,event,k):
     print('click:'+str(k))
 def click(self,event):
     if self.add_selected:
         self.add_selected=False
         self.window.config(cursor='arrow')
         self.add_tracker2(event.x,event.y)
     if self.is_right:
         self.menu.destroy()
         self.is_right = False
         self.resume()


 def select(self):
     for child in self.img_frame.winfo_children():
         child.destroy()
     #self.filename = filedialog.askopenfilename(initialdir="/home", title="Select a file",
     #                                           filetypes=(("Video files", "*.mp4"), ("all files", "*.*")))
     self.filename=eg.fileopenbox(title='choose video')
     if self.filename!=None:

         self.start_button.config(state="normal")
         self.video_source = self.filename
         self.set_dummy_vid()




 def pause(self):

      print('pause!!!!!!!!!!')
      self.is_paused = True
 def resume(self):
     print('resume!!!!!')
     self.is_paused = False
     if self.is_back:
        self.back_flag=True
        self.after_back = True
     self.is_back=False

 def back(self):
     self.is_paused=False
     print('back!!!!!')
     self.is_back=True
 def update(self):

     if self.video_selected:
         if self.is_paused==False:
             if self.is_back:


                 bb = self.frame_counter%self.backk
                 if bb==0:
                     bb=self.backk
                 if self.frame_counter-bb<0:
                     bb=self.frame_counter
                 self.frame_counter=self.frame_counter-bb
                 self.back_counter += bb
                 frame = self.frames[self.frame_counter]
                 ret = True
                 self.is_paused=True
             else:
                if self.back_flag:
                    if self.back_counter>0:
                        #print(self.back_counter)
                        self.frame_counter += 1
                        frame = self.frames[self.frame_counter]
                        self.back_counter-=1
                        ret=True
                    else:
                        self.back_flag=False
                        ret, frame = self.get_frame()
                        self.frames.append(frame)
                        self.frame_counter += 1


                else:
                    ret, frame = self.get_frame()
                    self.frames.append(frame)
                    self.frame_counter += 1
         else:
             #frame=self.Paused_frame
             frame = self.frames[self.frame_counter]
             ret=True

         if ret:
             #print(self.frame_counter)
             self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
             self.canvas.create_image(0, 0, image = self.photo, anchor = tkinter.NW)
             for rr in range(0,self.tracker_num):
                 if self.rect_xy[rr]!=None:
                     #print(self.cell_mid[rr][0], self.cell_mid[rr][1])
                     #self.canvas.create_rectangle(self.rect_xy[rr][0], self.rect_xy[rr][1], self.rect_xy[rr][2], self.rect_xy[rr][3])
                     self.canvas.create_rectangle(self.cell_mid[rr][0] - self.p_size, self.cell_mid[rr][1] - self.p_size, self.cell_mid[rr][0]+ self.p_size, self.cell_mid[rr][1] + self.p_size)
                     self.canvas.create_oval(self.cell_mid[rr][0] - 3, self.cell_mid[rr][1]  -3, self.cell_mid[rr][0]  + 3, self.cell_mid[rr][1]  + 3)
             for rr in range(0,len(self.prev_cell_mid)):
                 if self.prev_rect_xy[rr]:
                    self.canvas.create_rectangle(self.prev_cell_mid[rr][0] - self.p_size, self.prev_cell_mid[rr][1] - self.p_size,
                                              self.prev_cell_mid[rr][0] + self.p_size, self.prev_cell_mid[rr][1] + self.p_size)
                    self.canvas.create_oval(self.prev_cell_mid[rr][0] - 3, self.prev_cell_mid[rr][1] - 3, self.prev_cell_mid[rr][0] + 3,
                                     self.prev_cell_mid[rr][1] + 3)

             if self.frame_counter%self.step==0 and self.frame_counter>0:
                 #print(self.frame_counter)
                 if self.frame_counter>self.prev_label:
                    self.bar_labels[int(self.frame_counter/self.step)-1].config(bg='blue')
                 elif self.frame_counter<self.prev_label:
                    self.bar_labels[int(self.frame_counter / self.step) - 1].config(bg='white')
                 elif self.is_back:
                    self.bar_labels[int(self.frame_counter / self.step) - 1].config(bg='white')

                 self.prev_label=self.frame_counter
             if self.after_back:
                 self.bar_labels[int(self.frame_counter / self.step) - 1].config(bg='blue')
                 self.after_back = False
                 self.prev_label = self.frame_counter

         self.window.after(self.delay, self.update)
         #threading.Thread(target=self.window.after(self.delay, self.update)).start()
         if self.frame_counter==self.vid_length-1:
            self.cancel()


 def set_dummy_vid(self):
     dummy_vid = cv2.VideoCapture(self.video_source)
     if not dummy_vid.isOpened():
         raise ValueError("Unable to open video source", self.video_source)
     width = dummy_vid.get(cv2.CAP_PROP_FRAME_WIDTH)
     height = dummy_vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
     self.resize_factor=compute_resize_factor(height,width,self.heightpixels,self.widthpixels)
     print(self.resize_factor)
     width=int(width*self.resize_factor)
     height=int(height *self.resize_factor)
     dummy_canvas = tkinter.Canvas(self.img_frame, width=width, height=height)
     dummy_canvas.pack()

     ret, frame = dummy_vid.read()
     dummy_vid.release()
     if ret:
         frame1 = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
         frame=cv2.resize(frame1,(0,0),fx=self.resize_factor,fy=self.resize_factor)
         dummy_photo = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(frame))
         dummy_canvas.create_image(0, 0, image=dummy_photo, anchor=tkinter.NW)

     self.window.mainloop()



 def tag(self,cell_num):
     #choosed= eg.buttonbox("Choose the correct label? (if you close this window, Nothing will be saved)",
     #                            choices=["Healthy", "First type", "Second type","Unknown"])
     ch=my_Buttonbox(self.window)
     choosed=ch.label
     k=cell_num
     if type(choosed)!=int:
         self.center_files[k].close()
         self.label_files[k].close()
         self.activity_files[k].close()
         shutil.rmtree(self.cell_folders[k])
     else:
         label=int(choosed)
         self.label_files[k].write(str(label))
         self.center_files[k].close()
         self.label_files[k].close()
         self.activity_files[k].close()
         ff=open(self.cell_folders[k]+'/win_size.txt','w')
         ff.write(str(self.p_size)+'\n')
         ff.close()
     self.is_tagged[k]=True
     self.Icons[k].label.destroy()
     self.rect_xy[k]=None
     self.resume()
 def prev_tag(self,cell_num):
     #choosed= eg.buttonbox("Choose the correct label? (if you close this window, Nothing will be saved)",
     #                            choices=["Healthy", "First type", "Second type","Unknown"])
     ch=my_Buttonbox(self.window)
     choosed=ch.label
     if type(choosed)==int:
         k=cell_num
         label=int(choosed)
         label_file = open(self.prev_cell_folders[k] + '/label.txt', 'w')
         label_file.write(str(label) + '\n')
         label_file.close()
         ff=open(self.prev_cell_folders[k]+'/win_size.txt','w')
         ff.write(str(self.p_size)+'\n')
         ff.close()
         self.prev_Icons[k].label.destroy()
         self.prev_rect_xy[k]=False
     self.resume()
 def close_all(self):
     if self.video_selected==True:
         for i in range (0,self.tracker_num):
            if self.is_deleted[i]==False and self.is_tagged[i]==False:
                self.center_files[i].close()
                self.label_files[i].close()
                self.activity_files[i].close()
                shutil.rmtree(self.cell_folders[i])
     if self.vid!=None:
         self.vid.release()
         self.vid=None
 def cancel(self):
     self.video_selected = False
     for child in self.img_frame.winfo_children():
         child.destroy()
     for child in self.labelframe3.winfo_children():
         child.destroy()
     if self.vid!=None:
        self.vid.release()
     self.vid=None
     for i in range(0, self.tracker_num):
         if self.is_deleted[i]==False and self.is_tagged[i]==False :
            self.center_files[i].close()
            self.label_files[i].close()
            self.activity_files[i].close()
            shutil.rmtree(self.cell_folders[i])
     self.pause_button.config(state="disabled")
     self.back_button.config(state="disabled")
     self.up_button.config(state="disabled")
     self.down_button.config(state="disabled")
     self.resume_button.config(state="disabled")
     self.cancel_button.config(state="disabled")
     self.addt_button.config(state="disabled")
     self.start_button.config(state="normal")
     self.choose_button.config(state="normal")
     self.set_dummy_vid()




 def on_closing(self):

     self.close_all()
     self.window.destroy()


 def dnd_accept(self, source, event):
     return self
 def dnd_enter(self, source, event):
     k=self.name_dict[source.name]
     if k>=0:
        self.rect_xy[k]=None
     self.canvas.focus_set() # Show highlight border
     x, y,mx,my = source.where(self.canvas, event,self.frame_counter,self.center_files,self.name_dict,True,self.resize_factor,self.p_size)
     if k>=0:
         self.cell_mid[k][0]=mx
         self.cell_mid[k][1]=my
     else:
         k = self.prev_name_dict[source.name]
         self.prev_cell_mid[k][0]=mx
         self.prev_cell_mid[k][1]=my
         self.prev_rect_xy[k] = False

     x1, y1, x2, y2 = source.canvas.bbox(source.id)
     dx, dy = x2-x1, y2-y1
     self.dndid = self.canvas.create_rectangle(x, y, x+dx, y+dy)
     self.dnd_motion(source, event)

 def dnd_motion(self, source, event):
     k=self.name_dict[source.name]
     x, y,mx,my = source.where(self.canvas, event,self.frame_counter,self.center_files,self.name_dict,True,self.resize_factor,self.p_size)
     if k>=0:
         self.cell_mid[k][0]=mx
         self.cell_mid[k][1]=my
         self.rect_xy[k] =[self.cell_mid[k][0]-self.p_size,self.cell_mid[k][1]-self.p_size,self.cell_mid[k][0]+self.p_size,self.cell_mid[k][1]+self.p_size]
     else:
         k = self.prev_name_dict[source.name]
         self.prev_cell_mid[k][0]=mx
         self.prev_cell_mid[k][1]=my
         self.prev_rect_xy[k] = True
     x1, y1, x2, y2 = self.canvas.bbox(self.dndid)
     self.canvas.move(self.dndid, x-x1, y-y1)

 def dnd_leave(self, source, event):
     self.window.focus_set() # Hide highlight border
     self.canvas.delete(self.dndid)
     self.dndid = None

 def dnd_commit(self, source, event):
     k = self.name_dict[source.name]
     self.dnd_leave(source, event)
     x, y,mx,my  = source.where(self.canvas, event,self.frame_counter,self.center_files,self.name_dict,True,self.resize_factor,self.p_size)
     if k>=0:
         self.cell_mid[k][0]=mx
         self.cell_mid[k][1]=my
     else:
         k = self.prev_name_dict[source.name]
         self.prev_cell_mid[k][0]=mx
         self.prev_cell_mid[k][1]=my
     source.attach(self.canvas, x, y)


a=App1(tkinter.Tk(),'Activity Tracking tool')
a.window.mainloop()

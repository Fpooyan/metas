from tkinter import *
class my_Buttonbox(object):
    def __init__(self,parent,label_list):

        self.master = Toplevel(parent)
        lableframe=Frame(self.master )
        lableframe.grid(row=0, column=0)
        Bframe=Frame(self.master )
        Bframe.grid(row=1, column=0)
        self.label=Label(lableframe, text="Choose the correct label? (if you close this window, Nothing will be saved)")
        self.label.pack(pady=20,padx=10)
        self.b=[]
        l=len(label_list)
        for i in range(0,l):
            self.b.append(Button(Bframe, text=label_list[i],height=2, width=15,command=lambda a=i: self.e(a)))
            self.b[i].grid(row=int(i/4), column=(i%4))
        self.master.deiconify()
        self.master.wait_window()
    def e(self,k):
        self.label=k
        self.master.destroy()


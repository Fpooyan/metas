import tkinter
import PIL.Image, PIL.ImageTk
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import numpy as np
import os
from B_box2 import my_Buttonbox

def compute_resize_factor(h,w,win_h,win_w):
  print(h)
  print(win_h)
  rf=(0.85*win_h)/h
  while True:
    rf -= 0.01
    if rf*w<(0.95*win_w):
        print(rf)
        return rf
    rf-=0.01



class App1:
 def __init__(self, window, window_title):
     self.window = window
     self.window.title(window_title)
     self.widthpixels = self.window.winfo_screenwidth()
     self.heightpixels = self.window.winfo_screenheight()
     print(self.heightpixels,  self.widthpixels)
     self.resize_factor = None
     self.window.geometry('{}x{}'.format(self.widthpixels, self.heightpixels))
     self.button_frame = Frame(self.window)
     self.button_frame.pack(side=BOTTOM, fill=Y)
     self.button_frame2 = Frame(self.window)
     self.button_frame2.pack(side=LEFT, fill=Y)
     self.img_frame = Frame(self.window)
     self.img_frame.pack(anchor=tkinter.CENTER, expand=True)
     labelframe1 = LabelFrame(self.button_frame, text='')
     labelframe1.pack(fill="both", expand="yes")
     self.labelframe3 = LabelFrame(self.window, text='')
     self.labelframe3.pack(side=BOTTOM, fill=Y)
     labelframe2 = LabelFrame(self.button_frame2, text='Working panel')
     labelframe2.pack(fill="both", expand="yes")
     self.choose_button = Button(labelframe2, text='import folder', height=2, width=10, command=self.select)
     self.choose_button.grid(row=0, column=0)
     self.start_button = Button(labelframe2, text='start', height=2, width=10, command=self.start)
     self.start_button.grid(row=1, column=0)
     self.start_button.config(state="disabled")
     self.prev_button = Button(labelframe1, text='Prev', height=2, width=10,command=self.prev)
     self.prev_button.grid(row=0, column=1)
     self.prev_button.config(state="disabled")
     self.next_button = Button(labelframe1, text='Next', height=2, width=10,command=self.next)
     self.next_button.grid(row=0, column=3)
     self.next_button.config(state="disabled")
     self.edit_button = Button(labelframe1, text='Label', height=2, width=10,command=self.tag)
     self.edit_button.grid(row=0, column=2)
     self.edit_button.config(state="disabled")
     self.video_selected=False
     self.add_selected=False
     self.vid=None



 def update(self):
     self.frame_counter+=1
     if self.frame_counter<self.frame_numbers:
         img = PIL.Image.open(self.vid_frames[self.frame_counter])
         double_size = (int(img.size[0] * self.resize_factor), int(img.size[1] * self.resize_factor))
         frame = np.asanyarray(img.resize(double_size))
         self.photo = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(frame))
         self.canvas.create_image(0, 0, image=self.photo, anchor=tkinter.NW)
         self.window.after(25, self.update)





 def show_vid(self,vid_number):
     if vid_number>=0 and vid_number<self.vid_num:
         vid_path=self.video_source+'/'+self.dirs[vid_number]
         matches = []
         names = []
         for root, dirnames, filenames in os.walk(vid_path):
             for filename in filenames:
                 if filename.endswith(".jpg"):
                     matches.append(os.path.join(root, filename))
                     names.append(filename)
         self.vid_frames = matches
         self.frame_counter=-1
         self.frame_numbers=len(matches)
         self.canvas = tkinter.Canvas(self.img_frame, width=int(500), height=int(500))
         self.canvas.grid(row=0, column=0)
         self.update()







 def load_all(self):
     if self.canvas != None:
         for child in self.canvas.winfo_children():
             child.destroy()

     self.out_folder = self.video_folder + '/' + self.dirs[self.vid_counter]
     if os.path.exists(self.out_folder) == False:
         os.makedirs(self.out_folder)
     self.show_vid(self.vid_counter)


 def start(self):

     self.start_button.config(state="disabled")
     self.choose_button.config(state="disabled")
     for child in self.img_frame.winfo_children():
         child.destroy()
     self.next_button.config(state="normal")
     self.prev_button.config(state="normal")
     self.edit_button.config(state="normal")
     self.prev_label=0
     if os.path.exists('output') == False:
         os.makedirs('output')
     out_name = self.video_source.split('/')[-1]
     if os.path.exists('output/' + out_name) == False:
         os.makedirs('output/' + out_name)
     self.dirs = [d for d in os.listdir(self.video_source) if os.path.isdir(os.path.join(self.video_source, d))]
     self.vid_num=len(self.dirs)
     self.video_folder='output/' + out_name
     self.vid_counter = -1
     self.canvas=None
     self.resize_factor=5
     self.next()







 def select(self):
     for child in self.img_frame.winfo_children():
         child.destroy()
     #self.filename = filedialog.askopenfilename(initialdir="/home", title="Select a file",
     #                                           filetypes=(("Video files", "*.mp4"), ("all files", "*.*")))
     dirr = os.getcwd()
     self.filename = filedialog.askdirectory(initialdir=dirr,
                                             title="Select a folder",
                                             )
     if self.filename!=None:

         self.start_button.config(state="normal")
         self.video_source = self.filename
         self.label_source=self.filename+'_label'




 def next(self):
     self.vid_counter+=1
     if self.vid_counter==self.vid_num:
         self.vid_counter=0
     self.load_all()

 def prev(self):
     self.vid_counter-=1
     if self.vid_counter==-1:
         self.vid_counter = 0
     self.load_all()

 def tag(self):
     #choosed= eg.buttonbox("Choose the correct label? (if you close this window, Nothing will be saved)",
     #                            choices=["Healthy", "First type", "Second type","Unknown"])
     ch=my_Buttonbox(self.window,['Attack','Mitosis','Others'])
     choosed=ch.label
     if type(choosed)==int:
         label=int(choosed)
         ff = open(self.out_folder + '/label'+ '.txt', 'w')
         ff.write(str(label)+'\n')
         ff.close()
















 def on_closing(self):

     self.window.destroy()






a=App1(tkinter.Tk(),'Nimaad test App')
a.window.mainloop()

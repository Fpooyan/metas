# Metas

extractor.py >> This function gets sample centers as input and crops the sample from the specified video<br/>

false_pos_labeler.py >> This function gets positive sample centers as input and outputs false_pos centers(not overlapping with attack samples)<br/>

random_neg_labeler.py >> This function gets attack sample centers as input and randomly generates neg_centers(not overlapping with attack samples)<br/>

pos_locator.py >> This function get score map as input and outputs the center of samples with score more than a threshold<br/>
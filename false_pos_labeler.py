# This function gets positive sample centers as input and outputs false_pos centers(not overlapping with attack samples)
# Important parameters: 
#               winsize: The size of cropped square (each frame is considered as a square)
#               numofframes: The number of frames (video length)
#               spatial_ovrlap:  Maximum spatial overlap of a negative sample with an attack sample
#               temporal_ovrlap: Maximum temporal overlap of a negative sample with an attack sample

import cv2
import os
import random
import numpy as np
import json


def calc_ovrlp(l1,l2,winsize,numofframes,temporal_step_size,sp,temp):
    # This function checks the spatial and temporal overlap (Return True or False)
    spat_ovrlp = 0
    temp_ovrlp = 0

    h_e = min(l1[1]+winsize/2,l2[1]+winsize/2)
    h_s = max(l1[1]-winsize/2,l1[1]-winsize/2)

    w_e = min(l1[0]+winsize/2,l2[0]+winsize/2)
    w_s = max(l1[0]-winsize/2,l1[0]-winsize/2)

    if h_s<h_e and w_s<w_e:
        spat_ovrlp = (h_e-h_s)*(w_e-w_s) / (winsize**2)

    t_e=min(l1[2]+(numofframes*temporal_step_size) / 2, l2[2] + (numofframes*temporal_step_size) / 2)
    t_s=max(l1[2]-(numofframes*temporal_step_size) / 2, l2[2] - (numofframes*temporal_step_size) / 2)

    if t_s<t_e:
        temp_ovrlp = (t_e - t_s) / numofframes*temporal_step_size

    if spat_ovrlp> sp and temp_ovrlp> temp:
        return True
    return False

def save_false_pos_centers(path,dataset_path,stage_num,video_name,ex,winsize,numofframes,temporal_step_size,spatial_ovrlap,temporal_ovrlap,pos_centers,fppath):
    if os.path.exists(os.path.join(fppath,video_name+"_fp_centers.json")):
        print("False positive centers have already been generated for this video!")
    else:
        vid = cv2.VideoCapture(os.path.join(dataset_path,  video_name + ex))
        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        numframes = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
        print(width,height,numframes)
        attacks_centers = []
        for n_stage in range(0,stage_num):
            main_dir_A = path+'/stage_'+str(n_stage)+'/centers/Pos'
            if len(os.listdir(main_dir_A))>0:
                attackspath = os.path.join(main_dir_A,video_name+"_attack.json")
                if os.path.exists(attackspath)==True:
                    with open(attackspath,"r") as f:
                        attacks=json.load(f)
                    for attack in attacks.keys():
                        x,y,t = attacks[attack]
            
                        if y < int(winsize / 2):
                            y = int(winsize / 2)
                        if y + int(winsize / 2) >= height:
                            y = height - int(winsize / 2) - 1
                        if x < int(winsize / 2):
                            x = int(winsize / 2)
                        if x + int(winsize / 2) >= width:
                            x = width - int(winsize / 2) - 1
                        if t < numofframes / 2:
                            t = numofframes / 2
                        if t + numofframes / 2 > numframes:
                            t = numframes - numofframes / 2 - 1
                        attacks_centers.append([y,x,t])
        fp = {}
        counter=0
        for pos_sample in pos_centers:
            flag = True
            for attack in attacks_centers:
                if calc_ovrlp(pos_sample,attack,winsize,numofframes,temporal_step_size,spatial_ovrlap,temporal_ovrlap):
                    flag = False
                    break
            if flag==True:
                fp[str(counter)]=pos_sample
                counter+=1
        with open(os.path.join(fppath,video_name+"_fp_centers.json"),'w') as f:
            json.dump(fp,f)
            #for i in fp:
            #    f.write(str(i[0])+','+str(i[1])+','+str(i[2])+"\n")



#save_false_pos_centers('P:\\metas_new_codes\\dataset','t1',5,5,'P:\\metas_new_codes',0.5,0.5,[[20,20,23]])

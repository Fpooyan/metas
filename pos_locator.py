# This function get score map as input and outputs the center of samples with score more than a threshold
# Important parameters:  
#               cube_size: It is a tuple that contains the size of samples in three dimensions :[x_size,y_size,t_size]
#               x_stepsize: The step size in x dimension.
#               y_stepsize: The step size in y dimension.
#               t_stepsize: The step size in temporal dimension.
#               threshold: Under threshold samples consider as a positive sample

import os
import numpy as np


def pos_locator(scores,cube_size,x_stepsize,y_stepsize,t_stepsize,threshold):
    indices = np.argwhere(scores<threshold)
    out = []
    for i in range(indices.shape[0]):
        y,x,t = indices[i]
        x_mid = int((x * x_stepsize + x * x_stepsize + cube_size[0]) / 2)
        y_mid = int((y * y_stepsize + y * y_stepsize + cube_size[1]) / 2)
        t_mid = int((t * t_stepsize + t * t_stepsize + cube_size[2]) / 2)
        out.append([y_mid,x_mid,t_mid])
        print([y_mid,x_mid,t_mid])
    #with open("pos_locations.txt","w") as f:
    #    for o in out:
    #        f.write(str(o)+"\n")
    return out

